/*
 The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a car year goes here car make goes here car model goes here"*/
function carDetailsById(data, id) {
  // check the data  properly given correct or not
  if (Array.isArray(data) && typeof id === "number") {
    // retrive the data from the array
    for (let index = 0; index < data.length; index++) {
      let car_details = data[index];
      // find out the details for user required by using the id
      if (car_details.id == id) {
        return (
          "Car " +
          car_details.id +
          " is a car year " +
          car_details.car_year +
          " car make " +
          car_details.car_year +
          " car model " +
          car_details.car_model
        );
      }
    }
  } else {
    return "Data Insufficient ";
  }
}

// export the code
module.exports = { carDetailsById };
