// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function countOfCarsOlderThanTheYear2000(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    // TO inialize the count and store the count value
    let count = 0;
    for (let index = 0; index < data.length; index++) {
      // check the cars are older than the year 2000.
      if (data[index].car_year < 2000) {
        count++;
      }
    }
    return "no of cars manufuring older than the year 2000 :  " + count;
  } else {
    return " Data Insufficient ";
  }
}
// Export the Code
module.exports = { countOfCarsOlderThanTheYear2000 };
