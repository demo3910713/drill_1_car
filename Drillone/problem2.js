/* 
 The dealer needs the information on the last car in their inventory.
Execute a function to find what the make and model of the last car in the inventory is?
Log the make and model into the console in the format of:("Last car is a car make goes here car model goes here");
*/
function lastCarInformation(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    // Findout the last car information
    let car_details = data[data.length - 1];
    return (
      "Last Car is a car make " +
      car_details.car_year +
      " car model " +
      car_details.car_model
    );
  } else {
    return "Data Insufficient ";
  }
}
// Export the Code
module.exports = { lastCarInformation };
