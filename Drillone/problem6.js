// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
function onlyBNWandAudiInformation(data) {
  // check the data properly given correct or not
  if (Array.isArray(data)) {
    for (let index = 0; index < data.length; index++) {
      let name = data[index].car_make;
      // check the BNW and Audi cars by using the Data
      if (name === "Audi" || name === "BMW") {
        const carsInformation = JSON.stringify(data[index]);
        return carsInformation;
      }
    }
  } else {
    return " Data Insufficient ";
  }
}
// Export the code
module.exports = { onlyBNWandAudiInformation };
